$(document).ready(function () {
    $(window).on("load", function (e) {
        $(".navbar-nav .sub-menu").parent("li").append("<i class='icon'></i>");
    });

    $('.feedback_list').slick({
        centerMode: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: false,
        arrows: true
    });

    $('.info_list').slick({
        centerMode: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: false,
        arrows: true
    });

    $('.press_list').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: false,
        arrows: true
    });
});